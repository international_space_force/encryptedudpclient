#include <cstdio>
#include <cstring>
#include <vector>
#include "UDPClient.hpp"
#include "ConcurrentQueue.hpp"
#include "UDPServer.hpp"

const int DATALIMITSIZE = 3000;
const std::string KEY = "InternationalSpaceForce";

std::vector<char> DecryptData(std::vector<char> encryptedData, std::string key) {
    std::vector<char> decyptedData({});

    for(int index = 0; index < encryptedData.size(); index++) {
        if (encryptedData[index] != '\n') {
            int keyModuloIndex = index % key.size();
            decyptedData.push_back((char) encryptedData[index] ^ key[keyModuloIndex]);
        }
        else
            decyptedData.push_back(encryptedData[index]);
    }

    return decyptedData;
}

std::vector<char> encryptData(std::vector<char> rawData, std::vector<char> key) {
    std::vector<char> encryptedData({});

    for(int index = 0; index < rawData.size(); index++) {
        if (rawData[index] != '\n')
            encryptedData.push_back((char)rawData[index] ^ key[index % key.size()]);
        else
            encryptedData.push_back(rawData[index]);
    }

    return encryptedData;
}

std::vector<char> CharArrayToVector(char* inputData, int inputDataSize) {
    std::vector<char> result(inputData, inputData + inputDataSize);
    return result;
}

void VectorTocharArray(std::vector<char> inputData, char* outputData) {
    if(inputData.size() <= DATALIMITSIZE) {
        std::copy(inputData.begin(), inputData.end(), outputData);
    }
}

void SendDataToServerConnection(UDPClient& clientConnection, ConcurrentQueue<std::vector<char>>& dataQueue) {
    while(true) {
        std::vector<char> dataToSend;
        char buffer[DATALIMITSIZE];

        dataQueue.pop(dataToSend);
        VectorTocharArray(dataToSend, buffer);

        if (!clientConnection.SentDatagramToServerSuccessfully(buffer, DATALIMITSIZE)) {
            printf("error in sending the file\n");
        }
    }
}

void ReceivedDataFromServer(UDPServer& serverModuleConnection, ConcurrentQueue<std::vector<char>>& dataQueue) {
    while(true) {
        int receivedDataByteCount = 0;
        char buffer[DATALIMITSIZE];

        serverModuleConnection.GetDatagram(std::ref(receivedDataByteCount), buffer, DATALIMITSIZE);

        printf("Received from server: \n");
        for (int i = 0; i < 15; ++i)
            printf("%02X ", buffer[i]);
        printf("\n");

        std::vector<char> bufferVector(buffer, buffer + sizeof(buffer)/ sizeof(char));
        std::vector<char> decryptedDataArray = DecryptData(bufferVector, KEY);

        // printing some of the character to have a feel of decryption
        printf("Decrypted message: (First 15 characters)\n");
        for (int i = 0; i < 15; ++i)
            printf("%c ", decryptedDataArray[i]);
        printf("\n");
    }
}

int main()
{
    UDPClient client(4209, "127.0.0.1");
    UDPServer receiverFromServerModule(4210, "127.0.0.1");

    char file_buffer[DATALIMITSIZE], path[1024];
    ConcurrentQueue<std::vector<char>> dataQueue;

    std::thread sendData(SendDataToServerConnection, std::ref(client), std::ref(dataQueue));
    std::thread receiveData(ReceivedDataFromServer, std::ref(receiverFromServerModule), std::ref(dataQueue));

    while(true) {
        printf("Specify file name: \n");
        gets(path);

        // printf("%s\n", path);
        FILE* fp;
        fp = fopen(path, "r");
        if (fp == nullptr) {
            printf("file does not exist\n");
        }

        fseek(fp, 0, SEEK_END);
        size_t file_size = ftell(fp);
        fseek(fp, 0, SEEK_SET);

        if (fread(file_buffer, file_size, 1, fp) <= 0) {
            printf("unable to copy file into buffer\n");
            return(1);
        }

        std::vector<char> dataToSend = CharArrayToVector(file_buffer, file_size);
        dataQueue.push(dataToSend);

        bzero(file_buffer, sizeof(file_buffer));
    }

    sendData.join();
    receiveData.join();

    return 0;
}